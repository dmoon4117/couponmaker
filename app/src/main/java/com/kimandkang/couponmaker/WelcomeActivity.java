package com.kimandkang.couponmaker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class WelcomeActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_MAKER = 2001;
    public static final int REQUEST_CODE_MANAGE = 2002;
    public static final int REQUEST_CODE_SHARE = 2003;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);


        Button menu01Button = (Button) findViewById(R.id.button1);
        menu01Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MakerActivity.class);
                intent.putExtra("titleMsg", "고객 관리");

                startActivityForResult(intent, REQUEST_CODE_MAKER);
            }
        });

        Button menu02Button = (Button) findViewById(R.id.button2);
        menu02Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ManageActivity.class);
                intent.putExtra("titleMsg", "매출 관리");

                startActivityForResult(intent, REQUEST_CODE_MANAGE);
            }
        });

        Button menu03Button = (Button) findViewById(R.id.button3);
        menu03Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SharingActivity.class);
                intent.putExtra("titleMsg", "상품 관리");

                startActivityForResult(intent, REQUEST_CODE_SHARE);
            }
        });

    }
}
